import 'reflect-metadata';
import * as parseArgs from 'minimist';
import {useContainer} from 'typeorm';
import {Container} from 'typedi';
import {create as createConnection} from './src/persistance/db-connection';
import {IndexController} from './src/controller/index.controller';
import {readFile} from './src/util/file-system-util';
import {ImportProductConfigInterface} from './src/interface/import-product-config.interface';
import {ImportProductsConfigInterface} from './src/interface/import-products-config.interface';
import chalk from 'chalk';

(async () => {
  try {
    useContainer(Container);

    const args = parseArgs(process.argv),
      ormConfig = await readFile('../ormconfig.json');

    if (!args.mode) {
      throw new Error('Mode must by defined.');
    }

    await createConnection(JSON.parse(ormConfig.toString()));

    const indexController = Container.get(IndexController);

    switch (args.mode) {
      case 'import-product':
        let importProductConfig = <ImportProductConfigInterface>{
          url: args.url,
          strategy: args.strategy,
          category: args.category,
          forceImport: args['force-import'],
          save: args.save,
        };

        if (args['config-file'] && args['config-file'].length) {
          importProductConfig = <ImportProductConfigInterface>JSON.parse(await readFile(args['config-file']));
        }

        await indexController.importProductAction(importProductConfig);

        break;
      case 'import-products':
        if (args['config-file'] && args['config-file'].length) {
          let importProductsConfig = <ImportProductsConfigInterface>JSON.parse(await readFile(args['config-file']));

          await indexController.importProductsAction(importProductsConfig);
        } else {
          throw new Error('Mode import-products is available only with config-file parameter.');
        }

        break;
      case 'export-product':
        await indexController.exportProductAction(args.id);

        break;
      case 'export-products':
        let ids = [];

        if (args.ids && args.ids.length) {
          ids = args.ids.split(',');
        }

        await indexController.exportProductsAction(ids);

        break;
      case 'export-products-all':
        await indexController.exportAllProductsAction();

        break;
      case 'export-category':
        await indexController.exportCategoryAction(args.id);

        break;
      case 'export-categories-all':
        await indexController.exportAllCategoriesAction();

        break;
      default:
        throw new Error(`Mode ${args.mode} was not found.`);
    }
  } catch (error) {
    console.log(chalk.red(error));
  }

  process.exit();
})();
