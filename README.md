# Product scanner

### Requirements
- nodejs >= 8.11.3
- npm >= 5.6.0
- database server, eg. mariadb 10.2.16 (tested)

### Build
- Run `npm i`
- You must set up db connection, by creating a copy of ormconfig.json.dist file as ormconfig.json in the same directory. You must insert correct dbname, username, password and dbname. 
- Run `npm run build`

### Commands
- To build, run `npm run build`
- To test, run `npm run test`
- To develop, run `npm run dev`
- To execute, run `cd dist && node index.js --mode MODE`. 

  Modes: import-product, import-products, export-product, export-products, export-products-all, export-category, export-categories-all

  Options with `import-product`:
  - url: string - url from product will be imported.
  - strategy: string (xkom) - selected strategy, now only xkom is available.
  - category: string - category of product and attributes. If given category does not exist, it will be created.
  - save: bool|null - if specified product will be exported.
  - force-import: bool|null - if specified importer will not check if product already exists in database.
  - config-file: string|null - if specified importer will use only options defined in the file. HINT! Path to file must be in format like that: ../import-config.json
  
  Options with `import-products`:
    - config-file: string - is required. Importer will use only options defined in the file. HINT! Path to file must be in format like that: ../import-config.json
   
  Options with `export-product` or `export-category`:
  - id: number - id of product or category
  
- You can find saved product in `/dist/var`

### Examples
- `cd dist`
- To import from CLI: `node index.js --mode import-product --strategy xkom --url https://www.x-kom.pl/p/432960-smartfon-telefon-xiaomi-mi-mix-2s-6-64g-black.html0 --category test --save`
- To import with import config: `node index.js --mode import-product --config-file ../import-config.json`
- To export product: `node index.js --mode export-product --id 1`
- To export category: `node index.js --mode export-category --id 1`

### Warnings
- Using `import-products` mode can consume a lot of memory (sometimes) so be sure that you have minimum 2GB of free RAM.
