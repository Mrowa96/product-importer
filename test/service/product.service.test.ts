import {ProductService} from '../../src/service/product.service';
import {Product} from '../../src/entity/product';
import {Photo} from '../../src/entity/photo';
import {Attribute} from '../../src/entity/attribute';
import {CategoryRepository} from '../../src/repository/category.repository';
import {Category} from '../../src/entity/category';
import {AttributeOption} from '../../src/entity/attribute-option';
import {StrategyDataPersistenceService} from '../../src/service/strategy-data-persistence.service';
import {ProductRepository} from "../../src/repository/product.repository";

describe('ProductService', () => {
  let strategy;
  const categoryServiceMock = jest.fn<CategoryRepository>(() => ({
      updateAttributes: async function () {
        return;
      },
    })),
    productRepositoryMock = jest.fn<ProductRepository>(() => ({
      save: async function () {return;},
      findAllBySourceUrl: async function () {return <Product>{}},
    })),
    strategyDataPersistanceServiceMock = jest.fn<StrategyDataPersistenceService>(() => ({
      save: async function () {
        return;
      },
    })),
    categoryMock = new Category('test'),
    importProductService = new ProductService(new productRepositoryMock(), new categoryServiceMock(), new strategyDataPersistanceServiceMock());

  function testStrategy() {
    this.document = new Document();
    this.url = 'https://google.com';
    this.name = undefined;
    this.quantity = undefined;
    this.price = undefined;
    this.photos = undefined;
    this.reviews = undefined;
    this.attributesOptions = undefined;
  }

  beforeEach(() => {
    strategy = new testStrategy();
  });

  it('should create product from example strategy', done => {
    importProductService.import(strategy, categoryMock).then((product: Product) => {
      expect(product instanceof Product).toBeTruthy();
      expect(product.id).not.toBeNull();

      done();
    });
  });

  it('should create product with name', done => {
    strategy.name = 'Test';

    importProductService.import(strategy, categoryMock).then((product: Product) => {
      expect(product.name).toBe('Test');

      done();
    });
  });

  it('should create product with price', done => {
    strategy.price = 9.99;

    importProductService.import(strategy, categoryMock).then((product: Product) => {
      expect(product.price).toBe(9.99);

      done();
    });
  });

  it('should create product with photos', done => {
    strategy.photos = [<Photo>{}, <Photo>{}];

    importProductService.import(strategy, categoryMock).then((product: Product) => {
      expect(product.photos.length).toBe(2);

      done();
    });
  });

  it('should create product with quantity', done => {
    strategy.quantity = 25;

    importProductService.import(strategy, categoryMock).then((product: Product) => {
      expect(product.quantity).toBe(25);

      done();
    });
  });

  it('should create product with attributes options', done => {
    strategy.attributesOptions = [
      <AttributeOption>{
        id: 1,
        name: 'Test',
        attribute: <Attribute>{
          id: 1,
          name: 'Test',
          filterable: false,
          options: []
        }
      }
    ];

    importProductService.import(strategy, categoryMock).then((product: Product) => {
      expect(product.attributesOptions.length).toBe(1);

      done();
    });
  });
});
