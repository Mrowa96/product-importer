import {StrategyFactory} from '../../src/factory/strategy.factory';
import {Container} from 'typedi';

describe('StrategyFactory', () => {
  let factory = Container.get(StrategyFactory);

  beforeEach(() => {
    const strategyMapMock = jest.spyOn(factory, 'strategyMap', 'get');

    strategyMapMock.mockImplementation(() => {
      return {
        test: <ImportStrategyInterface>() => {
          return {
            set url(url: string) {}
          }
        }
      };
    });
  });

  it('should return valid strategy', () => {
    const strategy = factory.create('test', 'test');

    expect(strategy).toBeDefined();
  });

  it('should return TypeError', () => {
    try {
      factory.create('testaa', 'test');
    } catch (e) {
      expect(e).toBeDefined();
    }
  });
});
