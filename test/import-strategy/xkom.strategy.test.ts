import * as jsdom from 'jsdom';
import * as fs from 'fs';
import {XkomStrategy} from '../../src/import-strategy/xkom.strategy';

describe('Xkom strategy', () => {
  const strategy = new XkomStrategy();

  beforeAll(done => {
    strategy.url = './test/import-strategy/xkom.strategy.html';

    fs.readFile(strategy.url, (err: NodeJS.ErrnoException, data: Buffer) => {
      if (err) {
        throw err;
      }

      const dom = new jsdom.JSDOM(data);

      strategy.document = dom.window.document;

      done();
    });
  });

  it('should return correct name', () => {
    expect(strategy.name).toBe('Test');
  });

  it('should return correct price', () => {
    expect(strategy.price).toBe(2598);
  });

  it('should return valid photos', async () => {
    const photos = await strategy.photos;

    expect(photos.length).toBe(6);
    expect(photos[0].main).toBe(true);
    expect(photos[0].url).toBe(
      'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-mi-mix-2s-664g-black-432960,2018/6/pr_2018_6_4_15_39_46_232_06.jpg'
    );
  });

  it('should return valid attributes options', async () => {
    const attributesOptions = await strategy.attributesOptions;

    expect(attributesOptions.length).toBe(50);
  });
});
