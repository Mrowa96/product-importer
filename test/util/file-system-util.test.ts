import {readFile, writeFile} from '../../src/util/file-system-util';

test('Write example file successfully', async (done) => {
  await writeFile('./test/var/write-file-test', 'test');

  done();
});

test('Read example file successfully', async (done) => {
  const fileContent = await readFile('./test/util/read-file-test.json');

  done();
});

