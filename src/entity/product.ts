import {Photo} from './photo';
import {Category} from './category';
import {AttributeOption} from './attribute-option';
import {Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Attribute} from './attribute';
import {Review} from './review';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    charset: 'utf8mb4'
  })
  public name?: string;

  @Column('double')
  public price?: number;

  @Column()
  public quantity: number;

  @Column()
  public sourceUrl: string;

  @ManyToMany(type => AttributeOption)
  @JoinTable()
  public attributesOptions: Array<AttributeOption>;

  @ManyToMany(type => Photo)
  @JoinTable()
  public photos: Array<Photo>;

  @ManyToOne(type => Category, category => category.product)
  public category?: Category;

  @ManyToMany(type => Review)
  @JoinTable()
  public reviews: Array<Review>;

  private _attributes: Array<Attribute> = [];

  public get attributes(): Array<Attribute> {
    if (!this._attributes.length && this.attributesOptions && this.attributesOptions.length > 0) {
      for (const attributeOption of  this.attributesOptions) {
        let attribute = this._attributes.find((attribute: Attribute) => {
          return attribute.id === attributeOption.attribute.id;
        });

        if (!attribute) {
          attribute = attributeOption.attribute;
          attribute.options = [];

          this._attributes.push(attribute);
        }

        attributeOption.attribute = undefined;
        attribute.options.push(attributeOption);
      }
    }

    return this._attributes;
  }

  public toJSON(): object {
    return {
      id: this.id,
      name: this.name,
      price: this.price,
      quantity: this.quantity,
      category: this.category,
      photos: this.photos,
      reviews: this.reviews,
      attributes: this.attributes,
    };
  }
}
