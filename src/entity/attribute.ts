import {AttributeOption} from './attribute-option';
import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Attribute {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    charset: 'utf8mb4'
  })
  public name: string;

  @Column()
  public filterable: boolean;

  @OneToMany(type => AttributeOption, attributeOption => attributeOption.attribute)
  public options: Array<AttributeOption>;

  public constructor(name: string, filterable: boolean) {
    this.name = name;
    this.filterable = filterable;
  }
}
