import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Attribute} from './attribute';

@Entity()
export class AttributeOption {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    charset: 'utf8mb4'
  })
  public name: string;

  @ManyToOne(type => Attribute, attribute => attribute.options)
  public attribute: Attribute;

  public constructor(name: string) {
    this.name = name;
  }
}

