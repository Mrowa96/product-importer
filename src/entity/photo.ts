import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Photo {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public url: string;

  @Column()
  public main: boolean;

  public constructor(url: string, main: boolean = false) {
    this.url = url;
    this.main = main;
  }
}
