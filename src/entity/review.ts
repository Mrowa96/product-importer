import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Review {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public rating: number;

  @Column({
    charset: 'utf8mb4'
  })
  public authorName: string;

  @Column({
    type: 'datetime'
  })
  public date: string;

  @Column({
    type: 'text',
    charset: 'utf8mb4'
  })
  public content: string;

  public constructor(rating: number, authorName: string, date: string, content: string) {
    this.rating = rating;
    this.authorName = authorName;
    this.date = date;
    this.content = content;
  }
}
