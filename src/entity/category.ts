import {Attribute} from './attribute';
import {Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {Product} from './product';

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    charset: 'utf8mb4'
  })
  public name: string;

  @ManyToMany(type => Attribute)
  @JoinTable()
  public attributes: Array<Attribute>;

  @OneToMany(type => Product, product => product.category)
  public product: Product;

  public constructor(name: string) {
    this.name = name;
  }
}
