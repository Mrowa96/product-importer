import {Service} from 'typedi';
import {ProductService} from '../service/product.service';
import {writeFile} from '../util/file-system-util';
import {StrategyFactory} from '../factory/strategy.factory';
import {CategoryRepository} from '../repository/category.repository';
import {InjectRepository} from 'typeorm-typedi-extensions';
import {Product} from '../entity/product';
import {ProductRepository} from '../repository/product.repository';
import {Category} from '../entity/category';
import {ImportProductConfigInterface} from '../interface/import-product-config.interface';
import {ImportProductsConfigInterface} from '../interface/import-products-config.interface';
import {ImportConfigInterface} from '../interface/import-config.interface';
import chalk from 'chalk';
import {performance} from 'perf_hooks';

@Service()
export class IndexController {
  public constructor(
    @InjectRepository(Product) private productRepository: ProductRepository,
    @InjectRepository(Category) private categoryRepository: CategoryRepository,
    private strategyFactory: StrategyFactory,
    private productService: ProductService) {
  }

  public async importProductAction(config: ImportProductConfigInterface): Promise<void> {
    if (!config.url) {
      throw new Error('Url must by specified.');
    }

    if (!config.strategy) {
      throw new Error('Strategy must by specified.');
    }

    if (!config.category) {
      throw new Error('Category must by specified.');
    }

    const category = await this.categoryRepository.getCategoryByName(config.category, true);

    await this.importProduct(config, config.url, category);

    return;
  }

  public async importProductsAction(config: ImportProductsConfigInterface): Promise<void> {
    if (!Array.isArray(config.urls) || !config.urls.length) {
      throw new Error('Minimum one url must by specified.');
    }

    if (!config.strategy) {
      throw new Error('Strategy must by specified.');
    }

    if (!config.category) {
      throw new Error('Category must by specified.');
    }

    const category = await this.categoryRepository.getCategoryByName(config.category, true);

    for (const url of config.urls) {
      await this.importProduct(config, url, category);
    }

    return;
  }

  public async exportProductAction(id: number): Promise<void> {
    if (!id) {
      throw new Error('Id must by specified.');
    }

    const product = await this.productService.export(id);

    if (product) {
      await writeFile(`./var/product_${product.id}.json`, JSON.stringify(product));

      console.log(chalk.green(`Product with id ${product.id} was saved on the disk.`));
    } else {
      throw new Error(`Product cannot be found.`);
    }
  }

  public async exportProductsAction(ids: Array<number>): Promise<void> {
    if (!Array.isArray(ids) || !ids.length) {
      throw new Error('Minimum one id must by specified.');
    }

    const products: Array<Product> = [];

    for (const id of ids) {
      products.push(await this.productService.export(id));
    }

    if (products.length) {
      await writeFile(`./var/products_${ids.join('_')}.json`, JSON.stringify(products));

      console.log(chalk.green(`Products with ids ${ids.join(', ')} were saved on the disk.`));
    } else {
      throw new Error(`Cannot find any matching product.`);
    }
  }

  public async exportAllProductsAction(): Promise<void> {
    const products = await this.productService.exportAll(),
      ids = products.map((product: Product) => {
        return product.id;
      });

    await writeFile(`./var/products_${ids.join('_')}.json`, JSON.stringify(products));

    console.log(chalk.green(`Products with ids ${ids.join(', ')} were saved on the disk.`));
  }

  public async exportCategoryAction(id: number): Promise<void> {
    if (!id) {
      throw new Error('Id must by specified.');
    }

    const category = await this.categoryRepository.getCategoryById(id);

    if (category) {
      await writeFile(`./var/category_${category.id}.json`, JSON.stringify(category));

      console.log(chalk.green(`Category with id ${category.id} was saved on the disk.`));
    } else {
      throw new Error(`Category cannot be found.`);
    }
  }

  public async exportAllCategoriesAction(): Promise<void> {
    const categories = await this.categoryRepository.getAll(),
      ids = categories.map((category: Category) => {
        return category.id;
      });

    await writeFile(`./var/categories_${ids.join('_')}.json`, JSON.stringify(categories));

    console.log(chalk.green(`Categories with ids ${ids.join(', ')} were saved on the disk.`));
  }

  private async importProduct(config: ImportConfigInterface, url: string, category: Category): Promise<void> {
    let strategy = this.strategyFactory.create(config.strategy, url),
      startTime = performance.now(),
      endTime;

    if (strategy) {
      console.log(chalk.white(`Importing product from url ${url}.`));

      const product = await this.productService.import(strategy, category, config.forceImport);

      if (config.save) {
        console.log(chalk.white('Product was imported, now saving.'));

        await writeFile(`./var/product_${product.id}.json`, JSON.stringify(
          await this.productService.export(product.id)
        ));

        endTime = performance.now();

        console.log(chalk.green(`Product with id: ${product.id} was saved on the disk after ${endTime - startTime} milliseconds.\n`));
      } else {
        endTime = performance.now();

        console.log(chalk.green(`Product with id: ${product.id} was imported successfully after ${endTime - startTime} milliseconds.\n`));
      }
    }
  }
}
