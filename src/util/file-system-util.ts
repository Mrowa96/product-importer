import * as fs from 'fs';
import * as path from 'path';
import * as mkdirp from 'mkdirp';

export async function writeFile(filePath: string, fileContents: string): Promise<any> {
  const dirPath = path.dirname(filePath);

  return new Promise((resolve, reject) => {
    mkdirp(dirPath, (error) => {
      if (error) {
        reject(error);
      }

      fs.writeFile(filePath, fileContents, () => {
        resolve();
      });
    });
  });
}

export async function readFile(filePath: string): Promise<any> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (error, data) => {
      if (error) {
        reject(error);
      }

      resolve(data);
    });
  });
}
