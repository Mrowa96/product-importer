import {Category} from '../entity/category';
import {Product} from '../entity/product';
import {Attribute} from '../entity/attribute';
import {AttributeOption} from '../entity/attribute-option';
import {Photo} from '../entity/photo';
import {Review} from "../entity/review";
import {createConnection} from 'typeorm';

export async function create(config: any) {
  return await createConnection(Object.assign(config, {
    entities: [
      Product,
      Attribute,
      AttributeOption,
      Photo,
      Category,
      Review
    ]
  }))
}
