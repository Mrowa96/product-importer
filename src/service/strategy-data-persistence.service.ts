import {ImportStrategyInterface} from '../interface/import-strategy.interface';
import {Service} from 'typedi';
import {Review} from '../entity/review';
import {Photo} from '../entity/photo';
import {InjectRepository} from 'typeorm-typedi-extensions';
import {Repository} from 'typeorm';
import {Attribute} from '../entity/attribute';
import {AttributeOption} from '../entity/attribute-option';
import {keyInYN} from 'readline-sync';

@Service()
export class StrategyDataPersistenceService {
  public constructor(
    @InjectRepository(Photo) private photoRepository: Repository<Photo>,
    @InjectRepository(Review) private reviewRepository: Repository<Review>,
    @InjectRepository(Attribute) private attributeRepository: Repository<Attribute>,
    @InjectRepository(AttributeOption) private attributeOptionRepository: Repository<AttributeOption>) {
  }

  public async save(strategy: ImportStrategyInterface): Promise<void> {
    await this.savePhotos(strategy.photos);
    await this.saveReviews(strategy.reviews);
    await this.saveAttributesWithOptions(strategy.attributesOptions);

    return;
  }

  private async savePhotos(photos: Array<Photo>): Promise<void> {
    for (const photo of photos) {
      const savedPhoto = await this.photoRepository.findOne({
        where: {
          url: photo.url
        }
      });

      if (!savedPhoto) {
        await this.photoRepository.insert(photo);
      } else {
        photos[photos.indexOf(photo)] = savedPhoto;
      }
    }

    return;
  }

  private async saveReviews(reviews: Array<Review>): Promise<void> {
    if (reviews.length) {
      for (const review of reviews) {
        await this.reviewRepository.insert(review);
      }
    }

    return;
  }

  private async saveAttributesWithOptions(attributesOptions: Array<AttributeOption>): Promise<void> {
    for (const attributeOption of attributesOptions) {
      if (!attributeOption.attribute) {
        throw new TypeError('AttributeOption object should have Attribute object.');
      }

      const savedAttribute = await this.attributeRepository.findOne({
        where: {
          name: attributeOption.attribute.name
        }
      });

      if (!savedAttribute) {
        if (keyInYN(`${attributeOption.attribute.name} should be filterable?`)) {
          attributeOption.attribute.filterable = true;
        }

        await this.attributeRepository.insert(attributeOption.attribute);
      } else {
        await this.attributeRepository.save(savedAttribute);

        attributesOptions[attributesOptions.indexOf(attributeOption)].attribute = savedAttribute;
      }

      const savedAttributeOption = await this.attributeOptionRepository.findOne({
        where: {
          name: attributeOption.name
        },
        relations: ['attribute']
      });

      if (!savedAttributeOption) {
        await this.attributeOptionRepository.insert(attributeOption);
      } else {
        attributesOptions[attributesOptions.indexOf(attributeOption)] = savedAttributeOption;
      }
    }

    return;
  }
}
