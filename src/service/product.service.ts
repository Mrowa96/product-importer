import {ImportStrategyInterface} from '../interface/import-strategy.interface';
import {Product} from '../entity/product';
import {Service} from 'typedi';
import {InjectRepository} from 'typeorm-typedi-extensions';
import {Category} from '../entity/category';
import {CategoryRepository} from '../repository/category.repository';
import {StrategyDataPersistenceService} from './strategy-data-persistence.service';
import {ProductRepository} from "../repository/product.repository";
import fetch from 'node-fetch';
import * as jsdom from 'jsdom';

@Service()
export class ProductService {
  public constructor(
    @InjectRepository(Product) private productRepository: ProductRepository,
    @InjectRepository(Category) private categoryRepository: CategoryRepository,
    private strategyDataPersistenceService: StrategyDataPersistenceService) {
  }

  public async import(strategy: ImportStrategyInterface, category: Category, forceImport: boolean = false) {
    const existingProducts = await this.productRepository.findAllBySourceUrl(strategy.url);

    if (existingProducts.length && !forceImport) {
      const values: string = existingProducts.map((product: Product) => {
        return product.id;
      }).join(', ');

      if (existingProducts.length === 1) {
        throw new Error(`Product with given url was already imported. Id of this product is: ${values}.`);
      } else {
        throw new Error(`Products with given url were already imported. Ids of this products are: ${values}.`);
      }
    }

    const product = new Product(),
      response = await fetch(encodeURI(strategy.url));

    await response.text().then(async (html) => {
      const dom = new jsdom.JSDOM(html);

      strategy.document = dom.window.document;

      await this.strategyDataPersistenceService.save(strategy);

      product.name = strategy.name;
      product.price = strategy.price;
      product.quantity = strategy.quantity;
      product.sourceUrl = strategy.url;
      product.attributesOptions = strategy.attributesOptions;
      product.photos = strategy.photos;
      product.reviews = strategy.reviews;
      product.category = category;

      await this.categoryRepository.updateAttributes(category, product.attributes);
      await this.productRepository.save(product);
    });

    return product;
  }

  public async export(id: number): Promise<Product | undefined> {
    return await this.productRepository.findOne({
      where: {id},
      relations: [
        'photos',
        'category',
        'attributesOptions',
        'attributesOptions.attribute',
        'reviews'
      ]
    });
  }

  public async exportAll(): Promise<Product[]> {
    return await this.productRepository.find({
      relations: [
        'photos',
        'category',
        'attributesOptions',
        'attributesOptions.attribute',
        'reviews'
      ]
    });
  }
}
