import {EntityRepository, Repository} from "typeorm";
import {Product} from "../entity/product";

@EntityRepository(Product)
export class ProductRepository extends Repository<Product> {
  public async findAllBySourceUrl(sourceUrl: string): Promise<Product[]> {
    return await this.find({
      where: {sourceUrl}
    })
  }
}
