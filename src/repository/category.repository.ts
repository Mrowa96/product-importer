import {Category} from '../entity/category';
import {EntityRepository, Repository} from 'typeorm';
import {Attribute} from '../entity/attribute';

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {
  public async getCategoryById(id: number): Promise<Category> {
    return await this.findOne({
      where: {id},
      relations: ['attributes', 'attributes.options']
    });
  }

  public async getCategoryByName(name: string, createIfDoesNotExist: boolean = false): Promise<Category> {
    let category = await this.findOne({
      where: {name},
      relations: ['attributes', 'attributes.options']
    });

    if (!category && createIfDoesNotExist) {
      category = new Category(name);

      await this.insert(category);
    }

    return category;
  }

  public async getAll(): Promise<Category[]> {
    return await this.find({
      relations: ['attributes', 'attributes.options']
    });
  }

  public async updateAttributes(category: Category, attributes: Array<Attribute>): Promise<Category> {
    let filteredAttributes = attributes;

    if (Array.isArray(category.attributes) && category.attributes.length) {
      filteredAttributes = attributes.filter((attribute: Attribute) => {
        return !category.attributes.find((categoryAttribute: Attribute) => {
          return categoryAttribute.id === attribute.id;
        });
      });

      category.attributes.concat(filteredAttributes);
    } else {
      category.attributes = filteredAttributes;
    }

    await this.save(category);

    return category;
  }
}
