import {ImportStrategyInterface} from '../interface/import-strategy.interface';
import {XkomStrategy} from '../import-strategy/xkom.strategy';
import {Service} from 'typedi';

@Service()
export class StrategyFactory {
  private _strategyMap = {
    xkom: XkomStrategy
  };

  public get strategyMap() {
    return this._strategyMap;
  }

  public create(strategyName: string, strategyUrl: string): ImportStrategyInterface {
    let strategy;

    if (this.strategyMap.hasOwnProperty(strategyName)) {
      strategy = new this.strategyMap[strategyName]();
    } else {
      throw new Error(`Strategy with key ${strategyName} was not found.`);
    }

    strategy.url = strategyUrl;

    return strategy;
  }
}
