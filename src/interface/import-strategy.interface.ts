import {Photo} from '../entity/photo';
import {AttributeOption} from '../entity/attribute-option';
import {Review} from "../entity/review";

export interface ImportStrategyInterface {
  document: Document | undefined;

  url: string | undefined;

  name: string;

  price: number;

  quantity: number;

  photos: Array<Photo>;

  attributesOptions: Array<AttributeOption>;

  reviews: Array<Review>;
}
