import {ImportConfigInterface} from './import-config.interface';

export interface ImportProductConfigInterface extends ImportConfigInterface{
  url: string;
}
