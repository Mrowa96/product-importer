import {ImportConfigInterface} from './import-config.interface';

export interface ImportProductsConfigInterface extends ImportConfigInterface {
  urls: Array<string> | string;
}
