export interface ImportConfigInterface {
  strategy: string;
  category: string;
  save?: boolean;
  forceImport?: boolean;
}
