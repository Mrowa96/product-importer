import {ImportStrategyInterface} from '../interface/import-strategy.interface';
import {AttributeOption} from '../entity/attribute-option';
import {Photo} from '../entity/photo';
import {Review} from '../entity/review';
import {Attribute} from '../entity/attribute';

export class XkomStrategy implements ImportStrategyInterface {
  private _url: string | undefined;
  private _document: Document | undefined;
  private _name: string | undefined;
  private _price: number | undefined;
  private _quantity: number | undefined;
  private _photos: Array<Photo> | undefined;
  private _reviews: Array<Review> | undefined;
  private _attributesOptions: Array<AttributeOption> | undefined;

  public set url(url: string) {
    if (!this._url) {
      this._url = url;
    } else {
      throw new TypeError('Url was set up before.');
    }
  }

  public get url(): string | undefined {
    return this._url;
  }

  public set document(document: Document) {
    if (!this._document) {
      this._document = document;
    } else {
      throw new TypeError('Document was set up before.');
    }

    this._document = document;
  }

  public get document(): Document | undefined {
    return this._document;
  }

  public get name(): string {
    if (!this._name) {
      this._name = this.fetchName();
    }

    return this._name;
  }

  public get price(): number {
    if (!this._price) {
      this._price = this.fetchPrice();
    }

    return this._price;
  }

  public get quantity(): number {
    if (!this._quantity) {
      this._quantity = this.fetchQuantity();
    }

    return this._quantity;
  }

  public get photos(): Array<Photo> {
    if (typeof this._photos === 'undefined') {
      this._photos = this.fetchPhotos();
    }

    return this._photos;
  }

  public get reviews(): Array<Review> {
    if (typeof this._reviews === 'undefined') {
      this._reviews = this.fetchReviews();
    }

    return this._reviews;
  }

  public get attributesOptions(): Array<AttributeOption> {
    if (typeof this._attributesOptions === 'undefined') {
      this._attributesOptions = this.fetchAttributesOptions();
    }

    return this._attributesOptions;
  }

  private fetchName(): string {
    const nameElement = this.document.querySelector('.product-info h1');

    return nameElement.innerHTML.trim();
  }

  private fetchPrice(): number {
    const priceElement = this.document.querySelector('.product-infobox .price meta');

    return parseInt(priceElement.getAttribute('content'), 10);
  }

  private fetchQuantity(): number {
    return Math.floor((Math.random() * 100));
  }

  private fetchPhotos(): Array<Photo> {
    const photos: Array<Photo> = [],
      imgElements = Array.from(this.document.querySelectorAll('.product-slider-main a'));

    let isMain = true;

    for (const imgElement of imgElements) {
      const photoUrl = imgElement.getAttribute('href').trim(),
        photo = new Photo(photoUrl, isMain);

      photos.push(photo);

      if (isMain) {
        isMain = false;
      }
    }

    return photos;
  }

  public fetchAttributesOptions(): Array<AttributeOption> {
    const trElements = Array.from(this.document.querySelectorAll('.table-preview tr'));
    let attributesOptions: Array<AttributeOption> = [];

    for (const trElement of trElements) {
      const tdElements = trElement.querySelectorAll('td');

      if (tdElements.length >= 2) {
        const valuesElement: HTMLElement = <HTMLElement>tdElements[1],
          values = valuesElement.innerHTML.split('<br>'),
          options = this.fetchAttributeOptions(values),
          attributeName = trElement.querySelector('th').innerHTML.trim();

        const attribute = new Attribute(attributeName, false);

        for (const option of options) {
          option.attribute = attribute;
        }

        attributesOptions = attributesOptions.concat(options);
      }
    }

    return attributesOptions;
  }

  private fetchReviews(): Array<Review> {
    const reviews: Array<Review> = [],
      commentElements = Array.from(this.document.querySelectorAll('.comments-container .comment-details'));

    for (const commentElement of commentElements) {
      const review = new Review(
        getRating(commentElement),
        getAuthorName(commentElement),
        getDate(commentElement),
        getContent(commentElement),
      );

      reviews.push(review);
    }

    function getContent(element: Element): string {
      const contentElement = element.querySelector('.comment-content');
      let content = '';

      if (contentElement) {
        content = contentElement.innerHTML.trim();
      }

      return content;
    }

    function getDate(element: Element): string {
      const dateElement = element.querySelector('.comment-author span[itemProp="datePublished"]');
      let date = '';

      if (dateElement) {
        date = dateElement.innerHTML.trim();
        date = date.slice(0, 11) + date.slice(13);
      }

      return date;
    }

    function getAuthorName(element: Element): string {
      const authorNameElement = element.querySelector('.comment-author .comment-author-signature');
      let authorName = '';

      if (authorNameElement) {
        authorName = authorNameElement.innerHTML.trim();
        authorName = authorName.slice(0, authorName.length - 1);
      }

      return authorName;
    }

    function getRating(element: Element): number {
      const ratingElement = element.querySelector('.comment-rating meta[itemProp="ratingValue"]');
      let rating = 0;

      if (ratingElement) {
        const ratingValue = parseInt(ratingElement.getAttribute('content'));

        rating = Number.isNaN(ratingValue) ? 0 : ratingValue;
      }

      return rating;
    }

    return reviews;
  }

  private fetchAttributeOptions(values: Array<string>): Array<AttributeOption> {
    const options: Array<AttributeOption> = [];

    for (let value of values) {
      value = value.trim();

      if (value.length) {
        const attributeOption = new AttributeOption(value);

        options.push(attributeOption);
      }
    }

    return options;
  }
}
